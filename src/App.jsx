import "./App.css";
import { Provider } from "react-redux";
import Redux from "../components";
import store from "./app/store";

function App() {
  return (
    <>
      <Provider store={store}>
        <Redux />
      </Provider>
    </>
  );
}

export default App;
